/*
 * ClearURLs
 * Copyright (c) 2017-2021 Kevin Röbert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Preprocessor from '../preprocessors/preprocessor'
import RequestType from '../requestTypes/requestType'

/**
 * Models a rule.
 */
export default class Rule {
    private readonly _rule: RegExp
    private _active: boolean
    private readonly _requestTypes: RequestType[]
    private readonly _description: string
    private readonly _preprocessors: Preprocessor[]

    /**
     * Creates a new rule.
     *
     * @param rule - the rule as string
     * @param active - if the rule is active or not
     */
    constructor(rule: string, active: boolean = true) {
        this._rule = new RegExp(rule, 'gi')
        this._active = active
        this._requestTypes = []
        this._description = ''
        this._preprocessors = []
    }

    get value(): RegExp {
        return this._rule
    }

    get isActive(): boolean {
        return this._active
    }

    /**
     * Activates the rule.
     */
    public activate() {
        this._active = true
    }

    /**
     * Deactivates the rule.
     */
    public deactivate() {
        this._active = false
    }

    /**
     * Returns this rule as string.
     *
     * @returns the rule as string
     */
    public toString(): string {
        return this._rule.source
    }

    /**
     * Creates a new {@link Rule} object from the given json object.
     *
     * @param value the json object
     * @throws {@link IllegalArgumentException} if the given {@code value} is not valid
     */
    public static fromJSON(): any {
        //FIXME: implement correct serialization

        return undefined
    }
}